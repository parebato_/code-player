//function for texarea update

function updateOutput() {
  $('#outputPanel')
    .contents()
    .find('html')
    .html(
      '<head><style type="text/css">' +
        $('#cssPanel').val() +
        '</style></head><body>' +
        $('#htmlPanel').val() +
        '</body></html>'
    );

  //to evaluate js inside the outputPanel
  document
    .getElementById('outputPanel')
    .contentWindow.eval($('#javascriptPanel').val());
}

$('.toggleBtn').hover(
  function() {
    $(this).addClass('shaded');
  },
  function() {
    $(this).removeClass('shaded');
  }
);

$('.toggleBtn').click(function() {
  $(this).toggleClass('active');
  $(this).removeClass('shaded');

  var panelId = $(this).attr('id') + 'Panel';

  $('#' + panelId).toggleClass('hidden');

  var activePanels = 4 - $('.hidden').length;

  $('.panel').width($(window).width() / activePanels - 10);

  updateOutput();
});

$('.panel').height($(window).height() - $('#header').height() - 30);

//outputPanel change content from the html from htmlPanel

//HTML
$('textarea').on('change keyup paste', function() {
  updateOutput();
});

//CSS and Javascript
